package com.nyc.schools.domain

import com.nyc.schools.model.api.SchoolsApiClient
import com.nyc.schools.model.db.School
import com.nyc.schools.model.db.SchoolDao
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
internal class SchoolRepositoryTest {

    private val apiClient: SchoolsApiClient = mockk()
    private val schoolDao: SchoolDao = mockk()

    @Test
    fun `Given DB is not empty, Then returns DB value`() = runTest {
        val school: School = mockk()
        coEvery { schoolDao.getAllSchools() } returns listOf(school)
        val subject = SchoolRepository(apiClient, schoolDao)

        val result = subject.schoolList().first()

        assertEquals(Result.Success(listOf(school)), result)
    }
}