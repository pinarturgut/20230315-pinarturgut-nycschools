package com.nyc.schools.presentation

import com.nyc.schools.domain.Result
import com.nyc.schools.domain.SchoolRepository
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
internal class SchoolListViewModelTest {

    private val repository: SchoolRepository = mockk()

    @Before
    fun setup() {
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `Given school list success result, Then should emit Success state`() {
        every { repository.schoolList() } returns flowOf(Result.Success(listOf()))
        val subject = SchoolListViewModel(repository)

        assertEquals(Result.Success(listOf()), subject.uiState.value)
    }

    @Test
    fun `Given school list not received yet, Then should emit Loading state`() {
        every { repository.schoolList() } returns flow { }
        val subject = SchoolListViewModel(repository)

        assertEquals(Result.Loading, subject.uiState.value)
    }
}