package com.nyc.schools.presentation

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.nyc.schools.R
import com.nyc.schools.model.db.School
import com.nyc.schools.ui.theme.NycSchoolsTheme

@Composable
fun SchoolDetailsScreen(viewModel: SchoolDetailsViewModel = hiltViewModel()) {
    NycSchoolsTheme {
        viewModel.schoolState.value?.let { SchoolDetails(school = it) }
            ?: SchoolListLoading()
    }
}

@Composable
private fun SchoolDetails(school: School) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.surface)
            .padding(8.dp)
    ) {
        Text(
            text = school.schoolName,
            style = MaterialTheme.typography.h4,
            color = MaterialTheme.colors.onSurface
        )
        Spacer(modifier = Modifier.height(20.dp))
        Text(
            text = school.overview,
            color = MaterialTheme.colors.onSurface
        )
        Spacer(modifier = Modifier.height(20.dp))
        Text(
            text = stringResource(id = R.string.sat_scores),
            style = MaterialTheme.typography.h5,
            color = MaterialTheme.colors.onSurface
        )
        Spacer(modifier = Modifier.height(4.dp))
        Row {
            Text(
                text = stringResource(id = R.string.math_scores),
                color = MaterialTheme.colors.onSurface
            )
            Text(
                text = school.mathScore,
                color = MaterialTheme.colors.onSurface
            )
        }
        Row {
            Text(
                text = stringResource(id = R.string.writing_scores),
                color = MaterialTheme.colors.onSurface
            )
            Text(
                text = school.writingScore,
                color = MaterialTheme.colors.onSurface
            )
        }
        Row {
            Text(
                text = stringResource(id = R.string.reading_scores),
                color = MaterialTheme.colors.onSurface
            )
            Text(
                text = school.readingScore,
                color = MaterialTheme.colors.onSurface
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun SchoolDetailsPreview() {
    NycSchoolsTheme {
        SchoolDetails(
            School(
                dbn = "",
                schoolName = "SchoolName",
                address = "SchoolAddress",
                overview = "School Overview here",
                numberOfTestTakes = "",
                readingScore = "230",
                writingScore = "300",
                mathScore = "270"
            )
        )
    }
}