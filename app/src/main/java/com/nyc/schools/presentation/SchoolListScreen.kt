package com.nyc.schools.presentation

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.nyc.schools.R
import com.nyc.schools.domain.Result
import com.nyc.schools.model.db.School
import com.nyc.schools.ui.theme.NycSchoolsTheme
import kotlinx.coroutines.launch

@Composable
fun SchoolListScreen(
    viewModel: SchoolListViewModel = hiltViewModel(),
    navigateDetails: (String) -> Unit
) {
    NycSchoolsTheme {
        Scaffold(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colors.surface),
            topBar = {
                Box(modifier = Modifier
                    .fillMaxWidth()
                    .background(MaterialTheme.colors.background)
                ) {
                    Text(
                        modifier = Modifier.padding(12.dp),
                        text = stringResource(id = R.string.school_list_page_name),
                        style = MaterialTheme.typography.h5,
                        color = MaterialTheme.colors.onBackground
                    )
                }
            }
        ) {
            Surface(modifier = Modifier.padding(it)) {
                when (val result = viewModel.uiState.value) {
                    is Result.Error -> SchoolListErrorMessage()
                    Result.Loading -> SchoolListLoading()
                    is Result.Success -> SchoolList(result.schools, navigateDetails)
                }
            }
        }
    }
}

@Composable
private fun SchoolList(schools: List<School>, navigateDetails: (String) -> Unit) {
    val listState = rememberLazyListState()
    val coroutineScope = rememberCoroutineScope()
    val shouldShowUpArrow by remember {
        derivedStateOf { listState.firstVisibleItemIndex > 0 }
    }

    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        state = listState
    ) {
        itemsIndexed(schools) { index, school ->
            SchoolListItem(
                schoolName = school.schoolName,
                schoolAddress = school.address,
                onItemClick = { navigateDetails(school.dbn) }
            )
            if (index == schools.size -1) {
                Spacer(modifier = Modifier.height(95.dp))
            }
        }
    }
    AnimatedVisibility(
        visible = shouldShowUpArrow,
        enter = fadeIn(),
        exit = fadeOut(),
    ) {
        ScrollToTopButton {
            coroutineScope.launch {
                listState.animateScrollToItem(index = 0)
            }
        }
    }
}

@Composable
fun SchoolListLoading() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator()
    }
}

@Composable
private fun SchoolListErrorMessage() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.surface),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(id = R.string.network_error_message),
            color = MaterialTheme.colors.onSurface
        )
    }
}

@Preview(showBackground = true)
@Composable
fun SchoolListPreview() {
    NycSchoolsTheme {
        SchoolList(
            schools = listOf(
                School(
                    dbn = "",
                    schoolName = "SchoolName",
                    address = "SchoolAddress",
                    overview = "",
                    numberOfTestTakes = "",
                    readingScore = "",
                    writingScore = "",
                    mathScore = ""
                )
            ),
        ) { }
    }
}

@Composable
fun ScrollToTopButton(onClick: () -> Unit) {
    Box(
        Modifier
            .fillMaxSize()
            .padding(bottom = 20.dp), Alignment.BottomCenter
    ) {
        Button(
            onClick = { onClick() }, modifier = Modifier
                .shadow(10.dp, shape = CircleShape)
                .clip(shape = CircleShape)
                .size(65.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = MaterialTheme.colors.secondary,
                contentColor = MaterialTheme.colors.onPrimary
            )
        ) {
            Icon(Icons.Filled.KeyboardArrowUp, "arrow up")
        }
    }
}