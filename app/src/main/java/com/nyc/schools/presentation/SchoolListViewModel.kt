package com.nyc.schools.presentation

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nyc.schools.domain.Result
import com.nyc.schools.domain.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor(
    private val repository: SchoolRepository
) : ViewModel() {

    private val _uiState = mutableStateOf<Result>(Result.Loading)
    val uiState: State<Result> get() = _uiState

    init {
        viewModelScope.launch {
            repository.schoolList()
                .collect { _uiState.value = it }
        }
    }
}