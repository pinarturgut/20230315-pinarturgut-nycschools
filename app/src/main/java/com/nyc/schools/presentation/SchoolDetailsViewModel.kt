package com.nyc.schools.presentation

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nyc.schools.domain.Result
import com.nyc.schools.domain.SchoolRepository
import com.nyc.schools.model.db.School
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolDetailsViewModel @Inject constructor(
    private val repository: SchoolRepository,
    private val savedStateHandle: SavedStateHandle,
) : ViewModel() {

    private val _schoolState = mutableStateOf<School?>(null)
    val schoolState: State<School?> get() = _schoolState

    init {
        viewModelScope.launch {
            repository.schoolList()
                .filter { it is Result.Success }
                .map { result ->
                    result as Result.Success
                    result.schools.firstOrNull { it.dbn == savedStateHandle["dbn"] }
                }
                .collect { _schoolState.value = it }
        }
    }
}