package com.nyc.schools.domain

import com.nyc.schools.model.db.School

sealed class Result {
    object Loading : Result()
    data class Success(val schools: List<School>) : Result()
    data class Error(val throwable: Throwable) : Result()
}
