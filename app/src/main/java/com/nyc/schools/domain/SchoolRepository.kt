package com.nyc.schools.domain

import com.nyc.schools.model.api.SchoolsApiClient
import com.nyc.schools.model.db.School
import com.nyc.schools.model.db.SchoolDao
import dagger.Reusable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

@Reusable
class SchoolRepository @Inject constructor(
    private val schoolsApiClient: SchoolsApiClient,
    private val schoolDao: SchoolDao,
) {

    fun schoolList(): Flow<Result> = flow {
        val dbSchools = schoolDao.getAllSchools()

        if (dbSchools.isEmpty()) {
            try {
                val schools = downloadSchools()
                schoolDao.insertSchool(schools)
                emit(Result.Success(schools))
            } catch (t: Throwable) {
                emit(Result.Error(t))
            }
        }
        else {
            emit(Result.Success(dbSchools))
        }
    }

    private suspend fun downloadSchools(): List<School> = withContext(Dispatchers.IO) {
        val schoolInfoDefer = async { schoolsApiClient.getSchoolList() }
        val satScoresDefer = async { schoolsApiClient.getSATScores() }
        val schoolInfo = schoolInfoDefer.await()
        val satScore = satScoresDefer.await()

        schoolInfo.map { school ->
            val schoolSatScore = satScore.find { sat -> school.dbn == sat.dbn }
            School(
                dbn = school.dbn,
                schoolName = school.schoolName,
                address = school.address,
                overview = school.overview,
                numberOfTestTakes = schoolSatScore?.numberOfTestTakes ?: "N/A",
                readingScore = schoolSatScore?.readingScore ?: "N/A",
                writingScore = schoolSatScore?.writingScore ?: "N/A",
                mathScore = schoolSatScore?.mathScore ?: "N/A",
            )
        }
    }
}