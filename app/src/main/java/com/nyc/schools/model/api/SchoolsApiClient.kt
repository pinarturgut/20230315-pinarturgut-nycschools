package com.nyc.schools.model.api

import retrofit2.http.GET

interface SchoolsApiClient {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchoolList(): List<SchoolInfo>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSATScores(): List<SatScoresInfo>
}