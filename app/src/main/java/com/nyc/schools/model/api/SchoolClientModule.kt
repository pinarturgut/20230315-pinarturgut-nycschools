package com.nyc.schools.model.api

import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@InstallIn(SingletonComponent::class)
@Module
object SchoolClientModule {

    @[Provides Reusable]
    fun providesSchoolApiClient(): SchoolsApiClient =
        Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SchoolsApiClient::class.java)
}