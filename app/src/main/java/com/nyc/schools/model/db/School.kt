package com.nyc.schools.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class School(
    @PrimaryKey
    val dbn: String,
    val schoolName: String,
    val address: String,
    val overview: String,
    val numberOfTestTakes: String,
    val readingScore: String,
    val writingScore: String,
    val mathScore: String,
)