package com.nyc.schools.model.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(version = 1, entities = [School::class])
abstract class SchoolDatabase : RoomDatabase() {
    abstract fun getSchoolDao(): SchoolDao
}