package com.nyc.schools.model.api

import com.google.gson.annotations.SerializedName

data class SchoolInfo(
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("location")
    val address: String,
    @SerializedName("overview_paragraph")
    val overview: String,
)