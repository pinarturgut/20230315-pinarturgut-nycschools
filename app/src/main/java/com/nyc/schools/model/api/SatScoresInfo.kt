package com.nyc.schools.model.api

import com.google.gson.annotations.SerializedName

data class SatScoresInfo(
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("num_of_sat_test_takers")
    val numberOfTestTakes: String,
    @SerializedName("sat_critical_reading_avg_score")
    val readingScore: String,
    @SerializedName("sat_writing_avg_score")
    val writingScore: String,
    @SerializedName("sat_math_avg_score")
    val mathScore: String,
)