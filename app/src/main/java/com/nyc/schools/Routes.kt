package com.nyc.schools

enum class Routes {
    SchoolList,
    SchoolDetails
}