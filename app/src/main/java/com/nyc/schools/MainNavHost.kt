package com.nyc.schools

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.nyc.schools.presentation.SchoolDetailsScreen
import com.nyc.schools.presentation.SchoolListScreen

@Composable
fun MainNavHost(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
    startDestination: String = Routes.SchoolList.name
) {
    NavHost(
        modifier = modifier,
        navController = navController,
        startDestination = startDestination,
    ) {
        composable(Routes.SchoolList.name) {
            SchoolListScreen { dbn ->
                navController.navigate("${Routes.SchoolDetails}/$dbn")
            }
        }
        composable("${Routes.SchoolDetails}/{dbn}") {
            SchoolDetailsScreen()
        }
    }
}