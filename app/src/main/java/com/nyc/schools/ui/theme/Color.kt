package com.nyc.schools.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val NYC_Gray = Color(0xFFC7C7C7)
val NYCFlagBlue = Color(0xFF003585)
val NYCFlagOrange = Color(0xFFDA6703)
val NYCFlagBlueDark = Color(0xFF001E4B)

