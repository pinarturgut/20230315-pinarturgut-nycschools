package com.nyc.schools.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.nyc.schools.R

// Set of Material typography styles to start with

val Roboto_Black = FontFamily(
    Font(R.font.roboto_black),

)

val Roboto_Bold = FontFamily(
    Font(R.font.roboto_bold)
)

val Roboto_Light = FontFamily(
    Font(R.font.roboto_light)
)

val Roboto_Regular = FontFamily(
    Font(R.font.roboto_regular)
)

val Typography = Typography(
    h6 = TextStyle(
        fontFamily = Roboto_Regular,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp,
    ),
    h5 = TextStyle(
        fontFamily = Roboto_Bold,
        fontWeight = FontWeight.Normal,
        fontSize = 20.sp
    ),
    h4 = TextStyle(
        fontFamily = Roboto_Bold,
        fontWeight = FontWeight.Bold,
        fontSize = 30.sp
    ),
    subtitle2 = TextStyle(
        fontFamily = Roboto_Light,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp,
    )
    /* Other default text styles to override
    button = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.W500,
        fontSize = 14.sp
    ),
    caption = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    )
    */
)