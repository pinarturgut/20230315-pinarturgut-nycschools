package com.nyc.schools.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val DarkColorPalette = darkColors(
    primary = NYC_Gray,
    secondary = NYCFlagOrange,
    onSecondary = Color.White,
    background = NYCFlagBlue,
    onBackground = NYC_Gray,
    surface = NYCFlagBlueDark,
    onSurface = NYC_Gray,
    onPrimary = NYC_Gray
)

private val LightColorPalette = lightColors(
    primary = NYCFlagBlue,
    secondary = NYCFlagOrange,
    onSecondary = Color.White,
    background = NYCFlagOrange,
    onBackground = Color.White,
    surface = Color.White,
    onSurface = Color.Black,
)

@Composable
fun NycSchoolsTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}